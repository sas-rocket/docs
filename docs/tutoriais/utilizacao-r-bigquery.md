# Utilização básica do R com BigQuery


## Instale o pacote `bigrquery`

```{r}
install.packages("bigrquery")
```

## Ajuste as opções de autenticação

Para permitir que o _script_ seja executado não-interativamente, é importante que as seguintes opções estejam configuradas:

- `gargle_oauth_email`: E-mail ou domínio principal a ser aceito na autenticação
- `gargle_oauth_cache`: Diretório para armazenar as informações de autenticação

```{r}
options(
  #' Preferred Google domain
  gargle_oauth_email = "*@sas.com.br",
  #' Designate project-specific cache
  gargle_oauth_cache = "<path-to-cache-directory>"
)
```

## Leitura da credencial de acesso

Para executar as consultas no BigQuery é necessário ter o arquivo JSON com as informações de autenticação. Esse arquivo pode ser criado de acordo com as instruções [aqui](criando-credenciais-acesso-gcp.md). Assim, sabendo o caminho do arquivo criado, basta passá-lo como argumento para a função `bq_auth_configure`:
```{r}
#' Put this on every script using the authentication
bigrquery::bq_auth_configure(
  path = "<path-to-json-file>"
)
```

Será necessário pelo menos uma vez por projeto fazer o acesso via tela de consentimento (a configuração da tela de consentimento por aplicação é mostrada [aqui](criando-credenciais-acesso-gcp.md)). Para isso, execute o comando:
```{r}
bigrquery::bq_auth(email = "your-email")
```

E então será mostrada a tela de consentimento:
![Tela Consentimento GCP App](../static/img/tutoriais/screenshot-2024-04-22-174918.png)

Escolha uma conta e prossiga com as configurações. No final do processo você será avisado para retornar ao _prompt_ do R.

## Utilização Básica

Uma vez que tenha feito a primeira autenticação, as subsequentes poderão ser feitas não interativamente, desde que se mantenha o arquivo de _cache_:
```{r}
options(
  #' Preferred Google domain
  gargle_oauth_email = "*@sas.com.br",
  #' Designate project-specific cache
  gargle_oauth_cache = "<path-to-cache-directory>"
)

bigrquery::bq_auth_configure(
  path = "<path-to-json-file>"
)

# This is an example query using the public data from BigQuery
qry <- "
select 
  l.name, count(l.name) as nrepo 
from `bigquery-public-data.github_repos.languages`, unnest(language) as l 
group by l.name 
order by nrepo desc limit 10
"

# Returns a bq object
res <- bigrquery::bq_project_query("<project-id>", qry) 

bigrquery::bq_table_download(res)
```

O resultado desse _script_ deve ser mostrado conforme a figura abaixo, sem que ele seja interrompido pela tela de autenticação:
![Tela RStudio](../static/img/tutoriais/screenshot-2024-04-22-175824.png)
