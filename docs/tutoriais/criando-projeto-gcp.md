# Criar projeto no Google Cloud Computing

Para utilizar dados via [Google BigQuery](https://cloud.google.com/bigquery/) é necessário ter um projeto no [Google Cloud Computing](https://cloud.google.com/). Para isso, siga as seguintes etapas:

- Vá ao [Google Cloud Console](https://console.cloud.google.com/) e clique no botão de seleção de projeto:

![Tela do GCP Console](../static/img/tutoriais/screenshot-2024-04-22-114849.png)

- Clique na opção `Novo Projeto`:
![Tela do GCP Novo Projeto](../static/img/tutoriais/screenshot-2024-04-22-125125.png)

- Preencha o nome do projeto:
![Tela do GCP Cria Projeto](../static/img/tutoriais/screenshot-2024-04-22-125946.png)

- Clique em `Criar`. Você será redirecionado para o painel inicial do Console, e ao clicar no botão de seleção de projeto você verá o projeto que acabou de ser criado:

![Tela do GCP Console Fim](../static/img/tutoriais/screenshot-2024-04-22-130955.png)

Agora você já pode usar o BigQuery! :partying_face:

