# Atualização do Painel Inspira

1. Checar se o mapeamento das escolas Inspira estão corretos <> `Amanda Telles de Oliveira`

2. Pegar relação de escolas degustação e conveniadas `Rede-Inspira-SAS-Degustação.csv`

3. Envia o arquivo via python para o [Google Big Query](https://console.cloud.google.com/bigquery?project=sas-rocket)

```{python}
from google.oauth2 import service_account
import requests as rs
import time
import pandas as pd
import json
from urllib.request import urlopen
import pandas_gbq

#mapeamento_inspira = pd.read_excel(r'O:\SAS\EEx\EEx\Rocket\3. Produtos\15. Painel de Redes\4. ARQUIVOS\mapeamento_inspira.xlsx')
mapeamento_inspira = pd.read_csv(r'O:\SAS\EEx\EEx\Rocket\3. Produtos\15. Painel de Redes\4. ARQUIVOS\Rede-Inspira-SAS-Degustação.csv')

print(mapeamento_inspira)
print(len(mapeamento_inspira))

print('iniciou bigquery')
credentials = service_account.Credentials.from_service_account_info({
-- conseguir credenciais 
})

print('enviando')
pandas_gbq.to_gbq(mapeamento_inspira, 'dash_redes.pre_mapeamento_inspira', project_id='sas-rocket', if_exists='replace',credentials=credentials, api_method="load_csv")
print('fim')

```

4. Roda a consulta programada sob demanda [mapeamento_inspira](https://console.cloud.google.com/bigquery/scheduled-queries/locations/us/configs/665af112-0000-2c69-93bf-14c14ef136d8/runs?project=sas-rocket)

5. Roda as seguintes queries em ordem

**Obs: No caso da realização de mais alguma prova, será necessário incluir os ids da prova nas queries que pedem id de prova**

5.1 [fat_tri_avaliacoes](https://console.cloud.google.com/bigquery/scheduled-queries/locations/us/configs/62ffd540-0000-2c49-a5f9-94eb2c092708/runs?project=sas-rocket)
    
5.2 [fat_avaliacoes_redes_por_aluno_questao](https://console.cloud.google.com/bigquery/scheduled-queries/locations/us/configs/631c7b10-0000-2b26-9b59-14223bc451be/runs?project=sas-rocket)

5.3 [fat_aluno_questao_rede_inspira](https://console.cloud.google.com/bigquery/scheduled-queries/locations/us/configs/633080d5-0000-2889-a109-14223bb26e0a/runs?project=sas-rocket)

5.4 [fat_tri_rede_inspira](https://console.cloud.google.com/bigquery/scheduled-queries/locations/us/configs/63342227-0000-2bd2-8ca5-582429a6492c/runs?project=sas-rocket)

5.6 [base_redes](https://console.cloud.google.com/bigquery/scheduled-queries/locations/us/configs/62f8b67a-0000-21a4-963f-14223bc0c902/runs?project=sas-rocket)

6. Atualiza os dados do Power BI `O:\SAS\EEx\EEx\Rocket\3. Produtos\15. Painel de Redes\Painel Inspira\3. PROD\Painel SAS ENEM - Inspira`

    **Obs: checar na guia `Transformar Dados` se alguma query também tem filtro de id de prova. No caso de ter, atualizar também**


7. Atualizar o painel e publicar a versão mais recente neste [link do drive](https://drive.google.com/drive/folders/1t8XBe-kNH6EKO6Y1VXxW3kbz69gGtJdp?usp=sharing)

8. Ao atualizar o painel, comunicar via email para a `Priscila Queirós` ou alguém de RC, incluindo o link acima.
