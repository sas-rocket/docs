---
title: Ativar API para projeto no GCP
authors:
    - Victor Santos
tags: 
    - Google
    - GCP
---

- Entre em [Google API Console](https://console.developers.google.com/apis/library) e escolha o projeto no qual a API será ativada:
![Tela GCP Console API](../static/img/tutoriais/screenshot-2024-04-22-134519.png)

- No filtro da barra lateral esquerda, escolha a categoria `Big Data`:
![Tela GCP Console API Filtro](../static/img/tutoriais/screenshot-2024-04-22-134958.png)

- Na barra de busca, digite `"BigQuery API"` e selecione a opção mostrada:
![Tela GCP Console API Busca](../static/img/tutoriais/screenshot-2024-04-22-135620.png)

- Confirme se a API está ativada, e caso não, ative-a:
![Tela GCP Console API Ativação](../static/img/tutoriais/screenshot-2024-04-22-135140.png)

