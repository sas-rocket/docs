# Convenção de nomenclatura para tabelas 

| Categoria | Descrição | Composição | 
|-----------|-----------|------------|
| Código | Coluna cujo conteúdo expresse um código, cujo conteúdo não é obtido de uma sequence. Também é utilizada essa regra quando a coluna é uma FK, isto é, coluna herdada de outra tabela, e cuja PK é uma coluna `CO_+SEQ_+[NOME DO ATRIBUTO]`. | `CO_+[NOME DA COLUNA]` |
| Coordenada Geográfica | Coluna cujo conteúdo expressa um conjunto de coordenadas geográficas. Utilizado para plotar mapas e fazer referenciamento geográfico. | `CG_[NOME DA COLUNA]` |
| Data | Coluna cujo conteúdo expresse uma data do calendário civil. | `DT_+[NOME DA COLUNA]` |
| Descrição | Coluna cujo conteúdo é livre e em forma discursiva independente do tipo e tamanho utilizado (texto). | `DS_+[NOME DA COLUNA]` |
| Hora | Coluna cujo conteúdo expresse uma hora ou horário. | `HR_+[NOME DA COLUNA]` |
| Nome | Coluna cujo conteúdo é  de natureza alfanumérica e expressa um nome por extenso sendo composta de palavras, abreviaturas ou ambas. | `NO_+[NOME DA COLUNA]` |
| Número | Coluna cujo conteúdo é  representado por algarismos, não significando, necessariamente, que o tipo do campo tenha que ser possuir datatype de natureza numérica. | `NU_+[NOME DA COLUNA]` |
| Quantidade | Coluna cujo conteúdo expressa um quantitativo. O datatype deve ser de natureza numérica. | `QT_+[NOME DA COLUNA]` |
| Sigla | Coluna cujo conteúdo expressa uma sigla representativa de algo. O datatype deve ser de natureza alfnumérica. | `SG_+[NOME DA COLUNA]` |
| Situação ou Status | Coluna cujo conteúdo expressa a situação ou o status do registro ou de algum atributo. Deve ter uma lista de valores atrelada, que pode ser uma tabela de domínio ou uma check constraint. Obs.: Esta categoria de registro deve expressar um código, seja numérico ou alfanumérico, nunca um conteúdo por extenso/ discursivo. | `ST_+[NOME DA COLUNA]` |
| Tipo | Coluna cujo conteúdo expressa o tipo do registro ou de algum outro atributo. Deve ter uma lista de valores atrelada, que pode ser uma tabela de domínio ou uma check constraint. Obs.: Esta categoria de registro deve expressar um código, seja numérico ou alfanumérico, nunca um conteúdo por extenso/discursivo. | `TP_+[NOME DA COLUNA]` |
| Valor | Coluna cujo conteúdo expressa um valor numérico. O datatype deve ser de natureza numérica. | `VL_+[NOME DA COLUNA]` |
