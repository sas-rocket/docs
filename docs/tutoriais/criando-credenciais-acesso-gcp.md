---
title: Criando Credencial de Acesso ao GCP
authors:
    - Victor Santos
tags: 
    - Google
    - GCP
---

Ao utilizar _scripts_ que envolvem pouca ou nenhuma interação humana, a forma mais propriada de acessar o GCP é através de um _token_ de autorização de [serviço](https://cloud.google.com/compute/docs/access/service-accounts?hl=pt-br).

Para obter as credenciais de autorização, você vai precisar que a [API para o projeto](ativando-api-acesso-gcp.md) esteja ativada.

## Autenticação

Para criar as credenciais de acesso ao projeto:

- Entrar na [página de credenciais](https://console.developers.google.com/apis/credentials)
- Ir na opção `Credenciais` (barra lateral esquerda)
- Clicar em `Criar Credenciais > Conta de serviço`
![Tela Criação OAuth](../static/img/tutoriais/screenshot-2024-05-17-180947.png)
- Preencher as informações nos campos em branco:
![Tela Criação OAuth](../static/img/tutoriais/screenshot-2024-05-17-181103.png)
- No final, a tela resultante será parecida com esta:
![Tela Resultado](../static/img/tutoriais/screenshot-2024-05-17-181224.png)
- Clique no email criado para a conta de serviço, e a tela com detalhes será mostrada:
![Tela detalhes conta](../static/img/tutoriais/screenshot-2024-05-17-183625.png)
- Vá na aba `KEYS`, e então na opção `ADD KEYS`, e então `Criar nova chave`:
![Tela detalhes conta](../static/img/tutoriais/screenshot-2024-05-17-183859.png)
- Escolha a opção JSON:
![Tela detalhes conta JSON](../static/img/tutoriais/screenshot-2024-05-17-184131.png)

O arquivo JSON será então salvo localmente no seu computador

**ATENÇÃO**: se você por algum motivo perder essa chave, ela deve ser removida do painel do GCP, para evitar brecha de segurança.
