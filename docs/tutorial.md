- [Criando projeto no GCP](tutoriais/criando-projeto-gcp.md)
- [Criando credenciais de acesso no GCP](tutoriais/criando-credenciais-acesso-gcp.md)
- [Ativando API de projeto no GCP](tutoriais/ativando-api-acesso-gcp.md)
- [Usando BigQuery no R](tutoriais/utilizacao-r-bigquery.md)
- [Convenção de nomenclatura de colunas](tutoriais/convencao-colunas-tabelas.md)
- [Atualização do painel Inspira](tutoriais/painel-inspira.md)
- [Flashcards de Git](https://gitfichas.com/)

